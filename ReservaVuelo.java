import java.sql.*;
import java.io.*;
import java.util.Scanner;

public class ReservaVuelo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Bienvenido a la Agencia de viajes ViajaBarato");
            System.out.println("1- Dar de alta un vuelo");
            System.out.println("2- Dar de alta un pasajero");
            System.out.println("3- Reservar un vuelo");
            System.out.println("4- Modificar una reserva");
            System.out.println("5- Dar de baja una reserva");
            System.out.println("6- Salir");
            int opcion = sc.nextInt();
            sc.nextLine(); 
          do {
            

            switch (opcion) {
                case 1:
                   Connection con = DriverManager.getConnection("jdbc:mysql://localhost:33006/Vuelo", "root", "pass");
                   PreparedStatement ts = null;

                   String s = ("INSERT INTO Vuelos (origen, destino, fecha, capacidad) VALUES (?,?,?,?)");
                    System.out.println("Desde donde viajas?");
                    String origen = sc.nextLine();
                    System.out.println("A donde vas?");
                    String destino = sc.nextLine();
                    System.out.println("Cuando vas a ir? Recuerda introducir los datos asi yyyy/mm/dd");
                    String fecha = sc.nextLine();
                    System.out.println("De cuanta capacidad es el avion?");
                    int capacidad = sc.nextInt(); sc.nextLine();
                   try {
                    ts = con.prepareStatement(s);
                    ts.setString(1, origen);
                    ts.setString(2, destino);
                    ts.setString(3, fecha);
                    ts.setInt(4, capacidad);
                    ts.executeUpdate(); 
                   } catch (SQLException e) {
                    e.printStackTrace();
                   }
                    break;

                case 2:
                Connection con1 = DriverManager.getConnection("jdbc:mysql://localhost:33006/Vuelo", "root", "pass");
                PreparedStatement ts1 = null;
                String s1 = ("INSERT INTO Pasajeros (num_pasaporte,nom_pasajero) VALUES (?,?)");
                System.out.println("Digame su numero de pasaporte");
                String num_pasa = sc.nextLine();
                System.out.println("Digame su nombre");
                String nom_pasa = sc.nextLine();
                try {
                    ts1 = con1.prepareStatement(s1);
                    ts1.setString(1, num_pasa);
                    ts1.setString(2, nom_pasa);
                    ts1.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                    break;
                
                case 3:
                Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost:33006/Vuelo", "root", "pass");
                PreparedStatement ts2 = null;
                String s2 = ("INSERT INTO Reserva (id_vuelo,id_pasajero,id_reserva,n_asiento) VALUES (?,?,?,?)");
                Statement st = con2.createStatement();
                ResultSet rs = st.executeQuery("SELECT id_vuelo,origen,destino FROM Vuelos");
                while (rs.next()) {
                    int id = rs.getInt("id_vuelo");
                    String ori = rs.getString("origen");
                    String des = rs.getString("destino");
                    System.out.println("Este es un vuelo "+id+"-"+ori+"-"+des);
                }
                Statement st1 = con2.createStatement();
                ResultSet rs1 = st1.executeQuery("SELECT id_pasajero,nom_pasajero FROM Pasajeros");
                while (rs1.next()) {
                    int id1 = rs1.getInt("id_pasajero");
                    String nom = rs1.getString("nom_pasajero");
                    System.out.println("Estos son los pasajeros "+id1+"-"+nom);
                }
                System.out.println("Cual es la id del vuelo que quieres coger?");
                int idv = sc.nextInt();sc.nextLine();
                System.out.println("Cual es tu id de pasajero?");
                int idp = sc.nextInt();sc.nextLine();
                System.out.println("Dime un numero de reserva?");
                int idr = sc.nextInt();sc.nextLine();
                System.out.println("Dime un numero de asiento");
                int asiento = sc.nextInt();sc.nextLine();
                try {
                    ts2 = con2.prepareStatement(s2);
                    ts2.setInt(1, idv);
                    ts2.setInt(2, idp);
                    ts2.setInt(3, idr);
                    ts2.setInt(4, asiento);
                    ts2.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                    break;

                case 4:
                Connection con3 = DriverManager.getConnection("jdbc:mysql://localhost:33006/Vuelo", "root", "pass");
                PreparedStatement ts3 = null;
                Statement st4 = con3.createStatement();
                ResultSet rs4 = st4.executeQuery("SELECT id_reserva, n_asiento FROM Reserva");
                while (rs4.next()) {
                    int id4 = rs4.getInt("id_reserva");
                    int ass = rs4.getInt("n_asiento");
                    System.out.println("Estas son las reservas actuales "+id4+"-"+ass);
                }
                String s4 = "UPDATE Reserva SET n_asiento = ? WHERE id_reserva = ?";
                System.out.println("Por cual numero quieres cambiar tu asiento?");
                int assiento = sc.nextInt();sc.nextLine();
                System.out.println("Dime tu numero de reserva");
                int reser = sc.nextInt();sc.nextLine();
                try {
                    ts3 = con3.prepareStatement(s4);
                    ts3.setInt(1, assiento);
                    ts3.setInt(2, reser);
                    ts3.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                    break;

                case 5:
                Connection con5 = DriverManager.getConnection("jdbc:mysql://localhost:33006/Vuelo", "root", "pass");
                PreparedStatement ts5 = null;
                String del = "DELETE FROM Reserva WHERE id_reserva = ?";
                Statement st5 = con5.createStatement();
                ResultSet rs5 = st5.executeQuery("SELECT id_reserva FROM Reserva");
                while (rs5.next()) {
                    int id4 = rs5.getInt("id_reserva");
                    System.out.println("Estas son las reservas actuales "+id4);
                }
                System.out.println("Cual es la reserva que quieres borrar?");
                int r = sc.nextInt();sc.nextLine();
                try {
                    ts5 = con5.prepareStatement(del);
                    ts5.setInt(1, r);
                    ts5.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                    break;
        }
            System.out.println("1- Dar de alta un vuelo");
            System.out.println("2- Dar de alta un pasajero");
            System.out.println("3- Reservar un vuelo");
            System.out.println("4- Modificar una reserva");
            System.out.println("5- Dar de baja una reserva");
            System.out.println("6- Salir");
            opcion = sc.nextInt();
            sc.nextLine(); 
          } while (opcion < 6);
               
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sc.close(); 
        }
    }
}
